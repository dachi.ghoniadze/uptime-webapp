import redis
import subprocess
import shutil
import socket
import platform
from config import REDIS_HOST, REDIS_PORT


def check_redis_connection():
    try:
        r = redis.Redis(host=REDIS_HOST, port=int(REDIS_PORT))
        r.ping()
        return True
    except redis.ConnectionError:
        return False


def write_redis_data():
    pass


def check_service_status(service_name):
    if not shutil.which("systemctl"):
        return None
    try:
        status_output = subprocess.check_output(['systemctl', 'is-active', service_name])
        return status_output.strip() == b'active'
    except subprocess.CalledProcessError:
        return False


def check_redis_service_status():
    return check_service_status('redis')


def read_nginx_error_log():
    try:
        with open('/var/log/nginx/error.log', 'r') as file:
            return file.readlines()
    except FileNotFoundError:
        return ["Log file not found."]
    except PermissionError:
        return ["Permission denied to read log file."]


def read_nginx_access_log():
    try:
        with open('/var/log/nginx/access.log') as file:
            return file.readlines()
    except FileNotFoundError:
        return ["Log file not found."]
    except PermissionError:
        return ["Permission denied to read log file."]


def read_redis_log():
    try:
        with open('/var/log/redis/redis-server.log', 'r') as file:
            return file.readlines()
    except FileNotFoundError:
        return ["Log file not found."]
    except PermissionError:
        return ["Permission denied to read log file."]


def get_system_info():
    # Kernel and OS information
    kernel = subprocess.check_output(['uname', '-r']).decode('utf-8').strip()
    os_info = subprocess.check_output(['uname', '-o']).decode('utf-8').strip() if platform.system() != 'Darwin' else platform.system()
    
    # Hostname and IP address
    hostname = socket.gethostname()
    
    if platform.system() == 'Darwin':
        ip_address = subprocess.check_output(['ipconfig', 'getifaddr', 'en0']).decode('utf-8').strip()
    else:
        ip_address = socket.gethostbyname(hostname)
    
    # Uptime
    if platform.system() == 'Darwin':
        uptime = subprocess.check_output(['uptime']).decode('utf-8').strip().split(' up ')[1].split(',')[0]
    else:
        uptime = subprocess.check_output(['uptime', '-p']).decode('utf-8').strip()
    
    # CPU usage
    if platform.system() == 'Darwin':
        cpu_usage = subprocess.check_output(['ps', '-A', '-o', '%cpu']).decode('utf-8').split('\n')[1:-1]
        cpu_usage = str(round(sum(float(x) for x in cpu_usage if x.replace('.', '', 1).isdigit()), 2))
    else:
        cpu_usage = subprocess.check_output(['top', '-bn1']).decode('utf-8')
        cpu_usage_line = [line for line in cpu_usage.split('\n') if 'Cpu(s)' in line][0]
        cpu_usage = cpu_usage_line.split('%Cpu(s):')[1].split(',')[0].strip()
    
    # Memory usage
    if platform.system() == 'Darwin':
        vm_stat = subprocess.check_output(['vm_stat']).decode('utf-8')
        pages_free = int([line for line in vm_stat.split('\n') if 'Pages free' in line][0].split(':')[1].strip().replace('.', ''))
        pages_active = int([line for line in vm_stat.split('\n') if 'Pages active' in line][0].split(':')[1].strip().replace('.', ''))
        pages_inactive = int([line for line in vm_stat.split('\n') if 'Pages inactive' in line][0].split(':')[1].strip().replace('.', ''))
        pages_speculative = int([line for line in vm_stat.split('\n') if 'Pages speculative' in line][0].split(':')[1].strip().replace('.', ''))
        pages_wired = int([line for line in vm_stat.split('\n') if 'Pages wired down' in line][0].split(':')[1].strip().replace('.', ''))
        page_size = int(subprocess.check_output(['sysctl', 'hw.pagesize']).decode('utf-8').split(': ')[1].strip())
        mem_total = int(subprocess.check_output(['sysctl', 'hw.memsize']).decode('utf-8').split(': ')[1].strip())
        mem_used = (pages_active + pages_inactive + pages_wired + pages_speculative) * page_size
        mem_free = pages_free * page_size
        mem_total_mb = mem_total // (1024 * 1024)
        mem_used_mb = mem_used // (1024 * 1024)
        mem_percentage = round((mem_used / mem_total) * 100, 2)
    else:
        mem_usage = subprocess.check_output(['free', '-m']).decode('utf-8')
        mem_usage_line = mem_usage.split('\n')[1]
        mem_total_mb = int(mem_usage_line.split()[1])
        mem_used_mb = int(mem_usage_line.split()[2])
        mem_percentage = round((mem_used_mb / mem_total_mb) * 100, 2)
    
    # Disk usage
    disk_usage = subprocess.check_output(['df', '-h', '/']).decode('utf-8').strip().split('\n')[1].split()[4]
    
    return {
        'kernel': kernel,
        'os': os_info,
        'hostname': hostname,
        'ip_address': ip_address,
        'uptime': uptime,
        'cpu_usage': cpu_usage,
        'mem_usage': f'{mem_used_mb} MB / {mem_total_mb} MB ({mem_percentage}%)',
        'disk_usage': disk_usage
    }


def get_git_commit_id():
    try:
        commit_id = subprocess.check_output(['git', 'rev-parse', '--short', 'HEAD']).decode('utf-8').strip()
        return commit_id
    except subprocess.CalledProcessError:
        return "Unknown"
