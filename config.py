from dotenv import load_dotenv
import os

load_dotenv()

REDIS_HOST = os.getenv("REDIS_HOST")
REDIS_PORT = os.getenv("REDIS_PORT")
APP_HOST = os.getenv("APP_HOST")
APP_PORT = os.getenv("APP_PORT")
APP_DEBUG = os.getenv("APP_DEBUG")
