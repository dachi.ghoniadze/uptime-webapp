from flask import Flask, render_template
from misc_funcs import check_redis_connection, check_service_status, read_nginx_access_log, read_nginx_error_log, read_redis_log, get_system_info, get_git_commit_id, check_redis_service_status
from config import APP_HOST, APP_PORT, APP_DEBUG

app = Flask(__name__)

@app.route('/')
def index():
    redis_status = check_redis_connection()
    redis_service_status = check_redis_service_status()
    nginx_status = check_service_status('nginx')
    app_version = get_git_commit_id()
    return render_template("index.html", redis_status=redis_status, redis_service_status=redis_service_status, nginx_status=nginx_status, app_version=app_version)

@app.route('/system-stats')
def system_stats():
    system_info = get_system_info()
    app_version = get_git_commit_id()
    return render_template('system_stats.html', system_info=system_info, app_version=app_version)

@app.route('/service-logs')
def service_logs():
    nginx_logs = read_nginx_access_log()
    nginx_error_logs = read_nginx_error_log()
    redis_logs = read_redis_log()
    app_version = get_git_commit_id()
    return render_template('service_logs.html', nginx_logs=nginx_logs, nginx_error_logs=nginx_error_logs, redis_logs=redis_logs, app_version=app_version)

if __name__ == "__main__":
    app.run(host=APP_HOST, port=int(APP_PORT), debug=APP_DEBUG)
