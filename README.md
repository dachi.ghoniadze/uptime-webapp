# TBC Academy task application

This application shows a very basic information about your services, VM and Nginx logs. Here you can check whether the application is connected to Redis / is Nginx Service Up.

## Project structure

This is the overview of the project structure itself

```
flask-app/
├── app.py
├── config.py
├── requirements.txt
├── .env.example
├── templates/
│   ├── base.html
│   ├── index.html
│   ├── service_logs.html
│   └── system_stats.html
└── static/
    └── style.css
```

## Environment configuration

The application uses environment variables to configure various settings. Copy the `.env.example` file to `.env` and fill in the appropriate values.


* APP_HOST: The host address for the Flask application (default: 0.0.0.0).
* APP_PORT: The port for the Flask application (default: 5000).
* APP_DEBUG: Set to True or False to enable or disable debug mode.
* REDIS_HOST: The host address for the Redis server.
* REDIS_PORT: The port for the Redis server.

## Tasks

- You also need Redis on your system. Make sure to install it as well.
- This app might update a few more times, make sure to have some kind of automation to frequently check it (cron will do the trick maybe?)
